package org.firstinspires.ftc.teamcode.Test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Hardware.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.grab;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.wobble;

@TeleOp(name = "WobbleTest")
@Disabled
public class WobbleTest extends OpMode {

    DcMotor wobble;
    CRServo grab;
    @Override
    public void init() {
        wobble = hardwareMap.get(DcMotor.class, "sL");
        grab = hardwareMap.get(CRServo.class, "grab");
    }

    @Override
    public void loop() {
        if(gamepad1.right_trigger > 0.0) wobble.setPower(Range.clip(gamepad1.right_trigger, -0.5, 0.5));
        else if(gamepad1.left_trigger > 0.0) wobble.setPower(Range.clip(-gamepad1.left_trigger, -0.5, 0.5));
        else wobble.setPower(0.0);

        if(gamepad1.a) grab.setPower(1.0);
        else if(gamepad1.b) grab.setPower(-1.0);
        else grab.setPower(0.0);
    }
}
