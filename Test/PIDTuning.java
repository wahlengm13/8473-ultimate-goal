package org.firstinspires.ftc.teamcode.Test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;

@TeleOp(name = "PID Tuning")
@Disabled
public class PIDTuning extends LinearOpMode {
    private ElapsedTime timer = new ElapsedTime();
    private DriveTrain drive = new DriveTrain();
    private Hardware eagle = new Hardware();

    private Orientation angles;
    private double goalAngle, angleIE, lastAE;

    private int count = 0;
    private double lastDegree = 0;

    private int num = 0;

    private double kP = 0.0, kI = 0.0, kD = 0.0;

    private boolean dU = false, dD = false, startX = false;

    private double fR, fL, bR, bL;

    @Override
    public void runOpMode() throws InterruptedException {
        eagle.init(hardwareMap);

        angles = eagle.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        goalAngle = angles.firstAngle;

        waitForStart();

        timer.reset();
        while (opModeIsActive()) {
            angles = eagle.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

            if(gamepad1.start && gamepad1.x && !startX) if(num == 2) num = 0; else num++;

            if(gamepad1.dpad_up && !dU) {
                if(num == 0) kP += 0.01;
                else if(num == 1) kD += 0.01;
                else kI += 0.0001;
            } else if (gamepad1.dpad_down && !dD) {
                if(num == 0) kP -= 0.01;
                else if(num == 1) kD -= 0.01;
                else kI -= 0.001;
            }
            if(gamepad1.a) {
                drive.arcadeDrive(-0.3f, 0, PID());
            } else if (gamepad1.b) {
                drive.arcadeDrive(0.3f, 0, PID());
            } else if (gamepad1.x) {
                drive.arcadeDrive(0, -0.3f, PID());
            } else if (gamepad1.y) {
                drive.arcadeDrive(0, 0.3f, PID());
            } else
                drive.arcadeDrive(0, 0, PID());
            getButtons();
            updateTelemetry(PID());
        }
    }

    private float PID() {
        double angleE = goalAngle - angles.firstAngle;
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm = kP * angleE;
        double ITerm = kI * angleIE;
        double DTerm = kD * deltaAE;

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.35, Math.max(-0.35, correction));
        lastAE = angleE;

        if(Math.abs(correction) <= 0.01) return 0.0f;

        return (float)correction * -1;
    }

    private void updateTelemetry(double correction) {
        telemetry.addLine().addData("Values", kP + " " + kI + " " + kD).addData("Mode", num);
        telemetry.addLine().addData("IMU", getTrueAngle(angles.firstAngle));
        telemetry.update();
    }

    private void getButtons() {
        dU = gamepad1.dpad_up;
        dD = gamepad1.dpad_down;
        startX = gamepad1.start && gamepad1.x;
    }

    private double getTrueAngle(double degree) {
        if(lastDegree > 170 && degree < -170) count++;
        else if(lastDegree < -170 && degree > 170) count--;
        lastDegree = degree;
        return 360 * count + degree;
    }
}
