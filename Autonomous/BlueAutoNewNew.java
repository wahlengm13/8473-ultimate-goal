package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.WHITE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.color_sensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.grab;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.launch;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.wobble;

@Autonomous(name = "Blue Auto")
public class BlueAutoNewNew extends Auto{

    private ElapsedTime timer2 = new ElapsedTime();

    @Override
    public void information() {
        tensor = false;
    }

    @Override
    public void code() {
        if(pipeline.position == RingStackPipeline.RingStack.FOUR) {
            goToShoot();

            turningDrive(0.3, 0.75, 135, 2.25, 0.75);
            drive.off();

            grab.setPosition(0.8);
            drive(0.5f, 0.0f, 1);
            drive.off();

            turningDrive(0.5, -0.4, 0, 2, 0.0);
            drive.off();

            drive(-0.25f, 0.0f, 0.5);
            findLine(-0.55f);
            drive(0.0f, -0.55f, 0.8);
            drive(0.0f, -0.3f, 1.0);
            drive.off();
            grab.setPosition(0.0);
            sleep(500);

            findLine(0.55f);
            drive(-0.25f, 0.25f, 1);
            turningDrive(0.15, 0.5, 135, 1.5, 0);
            drive.off();

            grab.setPosition(0.6);
            sleep(500);

            while (color_sensor.alpha() < WHITE) {
                turningDrive(-0.15, -0.3, 135, 0.1, 0);
            }
            drive.off();

            wobble.setPower(-0.7);
            grab.setPosition(0.0);
            timer2.reset();
            while (timer2.seconds() < 0.6);
            wobble.setPower(0.0);
        } else if(pipeline.position == RingStackPipeline.RingStack.ONE) {
            goToShoot();

            turningDrive(0.1f, 0.5f, 90, 2.0, 0.75);
            drive(0.0f, 0.0f, 0.5);
            drive.off();

            grab.setPosition(0.8);
            sleep(500);

            drive(0.0f, 0.5f, 0.7);
            turningDrive(0.25, -0.5, 0, 1.5, 0);
            drive(0.0f, -0.5f, 1.0);
            drive(0.0f, -0.3f, 1.0);
            drive.off();

            grab.setPosition(0.0);
            sleep(500);
            findLine(0.5f);
            turningDrive(-0.35f, 0.35f, 55, 1.5, 0.0);
            drive.off();

            grab.setPosition(0.6);
            sleep(500);

            while (color_sensor.alpha() < WHITE) {
                turningDrive(0.2f, -0.35f, 55, 0.1, 0.0);
            }
            drive.off();

            wobble.setPower(-0.7);
            grab.setPosition(0.0);
            timer2.reset();
            while (timer2.seconds() < 0.6);
            wobble.setPower(0.0);
        } else {
            shooter.on();
            drive(-0.1f, 0.5f, 2.0);
            findLine(0.2f);
            findLine(-0.2f);
            drive(0.0f, -0.2f, 0.5);
            drive.off();

            sleep(250);

            launch.setPosition(0.75);
            sleep(150);
            launch.setPosition(1.0);
            sleep(1100);
            launch.setPosition(0.75);
            sleep(150);
            launch.setPosition(1.0);
            sleep(1100);
            launch.setPosition(0.75);
            sleep(150);
            launch.setPosition(1.0);
            shooter.off();
            drive.off();

            sleep(250);
            turningDrive(0.35, 0.20, 168, 2.25, 0.5);
            drive.off();
            grab.setPosition(0.8);
            sleep(250);
            turningDrive(-0.5, 0.0, 168, 1, 0);
            turningDrive(0.35, -0.35, 5, 1.75, 0);
            drive(0.0f, -0.5f, 1.0);
            drive(0.0f, -0.35f, 0.5);
            drive.off();

            grab.setPosition(0.0);
            sleep(500);

            turningDrive(-0.05, 0.65, 90, 2.25, 0);
            drive.off();

            grab.setPosition(0.6);
            sleep(500);

            drive(0.5f, 0.2f, 0.75);

            while (color_sensor.alpha() < WHITE) {
                turningDrive(-0.25, 0.25, 90, 0.1, 0.0);
            }
            drive.off();

            wobble.setPower(-0.7);
            grab.setPosition(0.0);
            timer2.reset();
            while (timer2.seconds() < 0.6);
            wobble.setPower(0.0);
        }
    }

    private void goToShoot() {
        telemetry.addData("Amount", pipeline.position);
        telemetry.update();

        shooter.on();
        drive(-0.24f, 0.35f, 2);
        while(color_sensor.alpha() < WHITE) {
            drive(0.2f, 0.25f);
        }
        drive.off();
        sleep(250);

        findLine(-0.2f);
        drive(0.0f, -0.2f, 0.5);
        drive.off();

        sleep(250);

        launch.setPosition(0.75);
        sleep(150);
        launch.setPosition(1.0);
        sleep(1100);
        launch.setPosition(0.75);
        sleep(150);
        launch.setPosition(1.0);
        sleep(1100);
        launch.setPosition(0.75);
        sleep(150);
        launch.setPosition(1.0);
        shooter.off();
        drive.off();
    }

    protected void drive(float x, float y) {
        updateAngles();
        drive.arcadeDrive(x, y, PID(false));
    }

    protected void turningDrive(double x, double y, double goal, double time, double liftTime) {
        goalAngle = goal;
        timer.reset();
        while (timer.seconds() < time) {
            updateAngles();
            drive.testDrive(x, y, PID(false), getTrueAngle(angles.firstAngle));
            if(timer.seconds() < liftTime) wobble.setPower(0.7);
            else wobble.setPower(0.0);
            telemetry.addData("Angle", getTrueAngle(angles.firstAngle));
            telemetry.update();
        }
    }

    protected void drive(float x, float y, double time) {
        timer.reset();
        while (timer.seconds() < time) {
            updateAngles();
            drive.arcadeDrive(x, y, PID(false));
            telemetry.addData("Angle", getTrueAngle(angles.firstAngle));
            telemetry.update();
        }
    }

    protected void drive(float x, float y, float z, double time) {
        timer.reset();
        while(timer.seconds() < time) {
            drive.arcadeDrive(x, y, z);
        }
    }

    protected void findLine(float y) {
        while (color_sensor.alpha() < WHITE) {
            updateAngles();
            drive.arcadeDrive(0, y, PID(false));
        }
    }

    private void updateAngles() {
        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }
}
