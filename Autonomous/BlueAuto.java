package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

import java.util.List;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.WHITE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.color_sensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.tfod;

@Autonomous (name = "Blue Auto Old Old")
@Disabled
public class BlueAuto extends Auto {
    @Override
    public void information() {
        tensor = true;
    }

    @Override
    public void code() {
        timer.reset();
        while (timer.seconds() < 0.8) drive.arcadeDrive(0, 0.3f, 0);
        drive.off();

        timer.reset();
        while (opModeIsActive() && (numRings == null || timer.seconds() < 1) && timer.seconds() < 2) {
            if (tfod != null) {
                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    for (Recognition recognition : updatedRecognitions) {
                        numRings = recognition.getLabel();
                        telemetry.addData("Number of Rings", numRings);
                        telemetry.update();
                    }
                }
            }
        }

        timer.reset();
        while (timer.seconds() < 1.4) {
            angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            drive.arcadeDrive(0.3f, 0, PID(false));
        }
        drive.off();

        if (numRings != null) {
            if (numRings.equalsIgnoreCase("SINGLE")) {
                goalAngle = 165.0;

                sleep(250);

                timer.reset();
                while (timer.seconds() < 1) {
                    drive.arcadeDrive(0, 0, -0.4f);
                }

                drive.off();
                sleep(250);

                timer.reset();
                while (timer.seconds() < 2) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, 0, PID(true));
                }

                while (color_sensor.alpha() < WHITE) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, -0.5f, PID(false));
                }

                timer.reset();
                while (timer.seconds() < 0.1) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, -0.5f, PID(false));
                }
                drive.arcadeDrive(0, 1f, PID(false));
                sleep(500);
                drive.off();

                goalAngle = 0.0;

                sleep(250);

                timer.reset();
                while (timer.seconds() < 1) {
                    drive.arcadeDrive(0, 0, 0.4f);
                }

                drive.off();
                sleep(250);

                timer.reset();
                while (timer.seconds() < 2) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, 0, PID(true));
                }

                drive.off();

//                shooting();
            } else if (numRings.equalsIgnoreCase("QUAD")) {
                goalAngle = 180.0;

                sleep(500);

                timer.reset();
                while (timer.seconds() < 1.2) {
                    drive.arcadeDrive(0, 0, -0.4f);
                }

                drive.off();
                sleep(250);

                timer.reset();
                while (timer.seconds() < 2) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, 0, PID(true));
                }

                while (color_sensor.alpha() < WHITE) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, -0.5f, PID(false));
                }

                timer.reset();
                while (timer.seconds() < 0.65) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, -0.5f, PID(false));
                }
                drive.arcadeDrive(0, 1f, PID(false));
                sleep(500);
                drive.off();

                sleep(100);

                goalAngle = 0.0;

                sleep(250);

                timer.reset();
                while (timer.seconds() < 1.2) {
                    drive.arcadeDrive(0, 0, 0.4f);
                }

                drive.off();
                sleep(250);

                timer.reset();
                while (timer.seconds() < 2) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(0, 0, PID(true));
                }

                drive.off();

                timer.reset();
                while (timer.seconds() < 1.4) {
                    angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                    drive.arcadeDrive(-0.3f, 0, PID(false));
                }
                drive.off();

//                shooting();
            }
        } else {
            drive.off();
            sleep(250);

            while (color_sensor.alpha() < WHITE) {
                angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                drive.arcadeDrive(0, 0.5f, PID(false));
            }

            timer.reset();
            while(timer.seconds() < 0.75) {
                angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                drive.arcadeDrive(0, 0.5f, PID(false));
            }

            timer.reset();
            while(timer.seconds() < 0.75) {
                angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                drive.arcadeDrive(0, -1, PID(false));
            }

            drive.arcadeDrive(0, 1f, 0);
            sleep(500);
            drive.off();

            sleep(250);

            timer.reset();
            while (timer.seconds() < 1.4) {
                angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
                drive.arcadeDrive(-0.3f, 0, PID(false));
            }
            drive.off();

//            shooting();
        }
    }
}
