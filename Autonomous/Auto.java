
package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Shooter;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;

import java.util.ArrayList;
import java.util.List;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.XYZ;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.LABEL_FIRST_ELEMENT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.LABEL_SECOND_ELEMENT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.TFOD_MODEL_ASSET;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.VUFORIA_KEY;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.WHITE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.grab;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kD;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kDI;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kDT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kII;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kIT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kP;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kI;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kPI;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kPT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.vuforia;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.tfod;

public abstract class Auto extends LinearOpMode {
    protected ElapsedTime timer = new ElapsedTime();
    protected DriveTrain drive = new DriveTrain();
    protected Shooter shooter = new Shooter();
    protected Hardware robot = new Hardware();
    protected Intake intake = new Intake();

    protected Orientation angles;
    protected static double goalAngle;
    protected static final double goalY = 28, goalX = 9;
    private double angleIE, lastAE;

    protected static final float mmPerInch = 25.4f;

    private static final float halfField = 72 * mmPerInch;
    private static final float quadField  = 36 * mmPerInch;

    private static final float mmTargetHeight   = (6) * mmPerInch;


    protected boolean targetVisible = false;

    protected OpenGLMatrix lastLocation = null;

    private int count = 0;
    private double lastDegree = 0.0;

    protected List<VuforiaTrackable> allTrackables = new ArrayList<>();

    RingStackPipeline pipeline;
    OpenCvCamera webcam;

    protected String numRings;
    protected boolean tensor;

    public abstract void information();
    public abstract void code();

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);

        grab.setPosition(0.0);

        vuforiaInit();
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam((WebcamName)vuforia.getCamera().getCameraName(), cameraMonitorViewId);

        pipeline = new RingStackPipeline();
        webcam.setPipeline(pipeline);

        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener() {
            @Override
            public void onOpened() {
                webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT);
            }
        });

        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        goalAngle = angles.firstAngle;

        information();
//        vuforiaInit();
        if(tensor) {
            tfodInit();
            if (tfod != null) tfod.activate();
        }
        while (!opModeIsActive()) {
            telemetry.addData("Analysis", pipeline.getAnalysis());
            telemetry.addData("Position", pipeline.position);
            telemetry.update();

            sleep(100);
        }
        
        waitForStart();
        webcam.closeCameraDevice();
        code();

        if (tfod != null) {
            tfod.shutdown();
        }
    }

    protected float PID(boolean turning) {
        double angleE = goalAngle - getTrueAngle(angles.firstAngle);
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm, ITerm, DTerm;

        PTerm = turning ? kPT * angleE : kP * angleE;
        ITerm = turning ? kIT * angleIE : kI * angleIE;
        DTerm = turning ? kDT * deltaAE : kD * deltaAE;

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.35, Math.max(-0.35, correction));
        lastAE = angleE;

        if(Math.abs(correction) <= 0.01) return 0.0f;

        return (float)correction * -1;
    }

    protected float PID(double y) {
        double angleE = goalY - y;
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm, ITerm, DTerm;

        PTerm = kPI * angleE;
        ITerm = kII * angleIE;
        DTerm = kDI * deltaAE;

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.3, Math.max(-0.3, correction));
        lastAE = angleE;

        if(Math.abs(correction) <= 0.01) return 0.0f;

        return (float)correction;
    }

    protected float PIDX(double x) {
        double angleE = goalX - x;
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm, ITerm, DTerm;

        PTerm = kPI * angleE;
        ITerm = kII * angleIE;
        DTerm = kDI * deltaAE;

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.3, Math.max(-0.3, correction));
        lastAE = angleE;

        if(Math.abs(correction) <= 0.01) return 0.0f;

        return (float)correction;
    }

//    protected void shooting() {
//        timer.reset();
//        while(timer.seconds() < 2) {
//            for(VuforiaTrackable trackable : allTrackables) {
//                if (((VuforiaTrackableDefaultListener)trackable.getListener()).isVisible()) {
//                    telemetry.addData("Visible Target", trackable.getName());
//                    targetVisible = true;
//
//                    OpenGLMatrix robotLocationTransform = ((VuforiaTrackableDefaultListener)trackable.getListener()).getUpdatedRobotLocation();
//                    if (robotLocationTransform != null) {
//                        lastLocation = robotLocationTransform;
//                    }
//                    break;
//                }
//            }
//
//            if (targetVisible) {
//                VectorF translation = lastLocation.getTranslation();
//                drive.arcadeDrive(PID(translation.get(1) / mmPerInch), 0, 0);
//            }
//            else {
//                telemetry.addData("Visible Target", "none");
//            }
//            telemetry.update();
//        }
//
//        drive.off();
//        sleep(250);
//
//        while (color_sensor.alpha() < WHITE) {
//            angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//            drive.arcadeDrive(0, -0.1f, PID(false));
//        }
//        drive.off();
//        shooter.on();
//
//        sleep(250);
//
//        goalAngle = LEFT_ANGLE;
//
//        timer.reset();
//        while(timer.seconds() < 2) {
//            angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//            telemetry.addData("Angle", angles.firstAngle);
//            telemetry.update();
//            drive.arcadeDrive(0, 0, PID(true));
//        }
//        drive.off();
//        sleep(250);
//
//        launch.setPosition(0.35);
//
//        sleep(250);
//
//        launch.setPosition(0.0);
//
//        goalAngle = MIDDLE_ANGLE;
//
//        timer.reset();
//        while(timer.seconds() < 2) {
//            angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//            telemetry.addData("Angle", angles.firstAngle);
//            telemetry.update();
//            drive.arcadeDrive(0, 0, PID(true));
//        }
//        drive.off();
//
//        launch.setPosition(0.5);
//
//        sleep(250);
//
//        launch.setPosition(0.0);
//
//        goalAngle = RIGHT_ANGLE;
//
//        intake.in();
//        timer.reset();
//        while(timer.seconds() < 2) {
//            angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//            telemetry.addData("Angle", angles.firstAngle);
//            telemetry.update();
//            drive.arcadeDrive(0, 0, PID(true));
//        }
//        intake.off();
//        drive.off();
//
//        launch.setPosition(0.25);
//
//        sleep(250);
//
//        while (color_sensor.alpha() < WHITE) {
//            drive.arcadeDrive(0, 1f, 0);
//        }
//        drive.off();
//    }

    public double getTrueAngle(double degree) {
        if(lastDegree > 170 && degree < -170) count++;
        else if(lastDegree < -170 && degree > 170) count--;
        lastDegree = degree;
        return 360 * count + degree;
    }

    private void vuforiaInit() {
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.cameraName = hardwareMap.get(WebcamName.class, "Webcam 1");
        parameters.vuforiaLicenseKey = VUFORIA_KEY;

        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        VuforiaTrackables targetsUltimateGoal = vuforia.loadTrackablesFromAsset("UltimateGoal");
        VuforiaTrackable blueTowerGoalTarget = targetsUltimateGoal.get(0);
        blueTowerGoalTarget.setName("Blue Tower Goal Target");
        VuforiaTrackable redTowerGoalTarget = targetsUltimateGoal.get(1);
        redTowerGoalTarget.setName("Red Tower Goal Target");
        VuforiaTrackable redAllianceTarget = targetsUltimateGoal.get(2);
        redAllianceTarget.setName("Red Alliance Target");
        VuforiaTrackable blueAllianceTarget = targetsUltimateGoal.get(3);
        blueAllianceTarget.setName("Blue Alliance Target");
        VuforiaTrackable frontWallTarget = targetsUltimateGoal.get(4);
        frontWallTarget.setName("Front Wall Target");

        allTrackables.addAll(targetsUltimateGoal);

        blueTowerGoalTarget.setLocation(OpenGLMatrix
                .translation(halfField, quadField, mmTargetHeight)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, XYZ, DEGREES, 90, 0 , -90)));

        targetsUltimateGoal.activate();
    }

    public void tfodInit() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minResultConfidence = 0.8f;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_FIRST_ELEMENT, LABEL_SECOND_ELEMENT);
    }

    public static class RingStackPipeline extends OpenCvPipeline {
        public enum RingStack {
            FOUR,
            ONE,
            NONE
        }

        static final Scalar BLUE = new Scalar(0, 0, 255);
        static final Scalar GREEN = new Scalar(0, 255, 0);

        /*
         * The core values which define the location and size of the sample regions
         */
        static final Point REGION1_TOPLEFT_ANCHOR_POINT = new Point(110,110);

        static final int REGION_WIDTH = 40;
        static final int REGION_HEIGHT = 40;

        final int FOUR_RING_THRESHOLD = 155;
        final int ONE_RING_THRESHOLD = 135;

        Point region1_pointA = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x,
                REGION1_TOPLEFT_ANCHOR_POINT.y);
        Point region1_pointB = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION1_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);

        /*
         * Working variables
         */
        Mat region1_Cb;
        Mat YCrCb = new Mat();
        Mat Cb = new Mat();
        int avg;

        // Volatile since accessed by OpMode thread w/o synchronization
        public volatile RingStack position = RingStack.FOUR;

        /*
         * This function takes the RGB frame, converts to YCrCb,
         * and extracts the Cb channel to the 'Cb' variable
         */
        void inputToCb(Mat input) {
            Imgproc.cvtColor(input, YCrCb, Imgproc.COLOR_RGB2YCrCb);
            Core.extractChannel(YCrCb, Cb, 1);
        }

        @Override
        public void init(Mat firstFrame) {
            inputToCb(firstFrame);

            region1_Cb = Cb.submat(new Rect(region1_pointA, region1_pointB));
        }

        @Override
        public Mat processFrame(Mat input) {
            inputToCb(input);

            int maxVal = 0;
            double[] maxPoint = {0, 0};
            for(int x = 40; x < 240; x += 5) {
                for(int y = 40; y < 160; y += 5) {
                    double[] vals = {x, y};
                    REGION1_TOPLEFT_ANCHOR_POINT.set(vals);
                    region1_pointA = new Point(
                            REGION1_TOPLEFT_ANCHOR_POINT.x,
                            REGION1_TOPLEFT_ANCHOR_POINT.y);
                    region1_pointB = new Point(
                            REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                            REGION1_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
                    region1_Cb = Cb.submat(new Rect(region1_pointA, region1_pointB));
                    if(maxVal < (int) Core.mean(region1_Cb).val[0]) {
                        maxVal = (int) Core.mean(region1_Cb).val[0];
                        maxPoint[0] = vals[0]; maxPoint[1] = vals[1];
                    }
                }
            }

            REGION1_TOPLEFT_ANCHOR_POINT.set(maxPoint);

            region1_pointA = new Point(
                    REGION1_TOPLEFT_ANCHOR_POINT.x,
                    REGION1_TOPLEFT_ANCHOR_POINT.y);
            region1_pointB = new Point(
                    REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                    REGION1_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
            region1_Cb = Cb.submat(new Rect(region1_pointA, region1_pointB));

            avg = (int) Core.mean(region1_Cb).val[0];

            Imgproc.rectangle(
                    input, // Buffer to draw on
                    region1_pointA, // First point which defines the rectangle
                    region1_pointB, // Second point which defines the rectangle
                    BLUE, // The color the rectangle is drawn in
                    2); // Thickness of the rectangle lines

            position = RingStack.FOUR; // Record our analysis
            if(avg > FOUR_RING_THRESHOLD) {
                position = RingStack.FOUR;
            } else if (avg > ONE_RING_THRESHOLD) {
                position = RingStack.ONE;
            } else {
                position = RingStack.NONE;
            }

            return input;
        }

        public int getAnalysis() {
            return avg;
        }
    }
}
