package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.launch;

@Autonomous(name = "Shooter")
@Disabled
public class ShooterTest extends Auto {
    @Override
    public void information() {

    }

    @Override
    public void code() {
        shooter.on();
        sleep(1500);
        launch.setPosition(0.3);
        sleep(1000);
    }
}
