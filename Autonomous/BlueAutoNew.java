package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

import java.util.List;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.WHITE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.color_sensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.launch;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.tfod;

@Autonomous (name = "Blue Auto Old")
@Disabled
public class BlueAutoNew extends Auto {
    @Override
    public void information() {
        tensor = true;
    }

    @Override
    public void code() {
        drive(0, 0.6f, 0, 0.6);
        drive.off();

        timer.reset();
        while (opModeIsActive() && (numRings == null || timer.seconds() < 1) && timer.seconds() < 2) {
            if (tfod != null) {
                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    for (Recognition recognition : updatedRecognitions) {
                        numRings = recognition.getLabel();
                        telemetry.addData("Number of Rings", numRings);
                        telemetry.update();
                    }
                }
            }
        }

        drive(0.6f, 0,1.4);
        drive.off();
        sleep(500);

        if (numRings != null) {
            if (numRings.equalsIgnoreCase("SINGLE")) {
                drive(0, 0, -0.7f,1);
                drive.off();
                sleep(250);

                turn(2.0, 165.0);
                findLine(-0.8f);
                drive(0, -0.8f, 0.1);
                drive(0, 1, 0.25);
                drive.off();
                sleep(250);

                drive(0, 0, 0.7f, 1);
                drive.off();
                sleep(250);

                turn(2.0, 0.0);
                drive.off();

                shooting2();
            } else if (numRings.equalsIgnoreCase("QUAD")) {
                drive(0, 0, -0.7f, 1.2);
                drive.off();
                sleep(250);

                turn(2.0, 180.0);
                findLine(-0.8f);
                drive(0, -0.8f, 0.95);
                drive(0, 1, 0.5);
                drive.off();
                sleep(250);

                drive(0, 0, 0.7f, 1.2);
                drive.off();
                sleep(500);

                turn(2.0, 0.0);
                drive.off();
                sleep(250);

                drive(-0.6f, 0, 1.4);
                drive.off();

                shooting2();
            }
        } else {
            findLine(0.8f);
            drive(0, 0.8f, 1.0);
            drive(0, -1, 0.5);
            drive(0, 1, 0, 0.5);
            drive.off();
            sleep(250);

            drive(-0.6f, 0, 1.4);
            drive.off();

            shooting2();
        }
    }
    protected void shooting2() {
        timer.reset();
        while(timer.seconds() < 2) {
            for(VuforiaTrackable trackable : allTrackables) {
                if (((VuforiaTrackableDefaultListener)trackable.getListener()).isVisible()) {
                    telemetry.addData("Visible Target", trackable.getName());
                    targetVisible = true;

                    OpenGLMatrix robotLocationTransform = ((VuforiaTrackableDefaultListener)trackable.getListener()).getUpdatedRobotLocation();
                    if (robotLocationTransform != null) {
                        lastLocation = robotLocationTransform;
                    }
                    break;
                }
            }

            if (targetVisible) {
                VectorF translation = lastLocation.getTranslation();
                drive.arcadeDrive(PID(translation.get(1) / mmPerInch), 0, 0);
                telemetry.addData("", PID(translation.get(1) / mmPerInch));
                telemetry.update();
            }
            else {
                telemetry.addData("Visible Target", "none");
            }
            telemetry.update();
        }
        drive.off();
        sleep(250);

        shooter.on();
        timer.reset();
        findLine(-0.4f);
        drive.off();
        sleep(250);
        while(timer.seconds() < 2);

        launch.setPosition(0.75);
        sleep(250);
        launch.setPosition(1.0);
        sleep(1000);
        launch.setPosition(0.75);
        sleep(250);
        launch.setPosition(1.0);
        intake.in();
        sleep(1000);
        intake.off();
        launch.setPosition(0.25);
        sleep(250);
        launch.setPosition(0.0);

        if(numRings != null) {
            if (numRings.equalsIgnoreCase("SINGLE")) {
                intake.in();
                drive(0, -0.8f, 1.0);
                drive.off();
                sleep(250);
                drive(0, 0.8f, 0.8);
                intake.off();
                drive.off();
                launch.setPosition(0.25);
                sleep(250);
            } else if (numRings.equalsIgnoreCase("QUAD")) {
                intake.in();
                drive(0, -1, 0.6);
                drive(0, 1, 0.3);
                drive.off();
                sleep(250);
                drive(0, -0.6f, 0.4);
                drive.off();
                sleep(250);
                drive(0, 0.6f, 0.6);
                intake.off();
                drive.off();
                sleep(250);
                launch.setPosition(0.25);
                sleep(250);
            }
        }

        findLine(0.75f);
        drive.off();
    }


    protected void turn(double time, double goalAngle) {
        Auto.goalAngle = goalAngle;
        timer.reset();
        while (timer.seconds() < time) {
            updateAngles();
            drive.arcadeDrive(0, 0, PID(true));
            telemetry.addData("", Auto.goalAngle);
            telemetry.addData("", PID(true));
            telemetry.update();
        }
    }

    protected void drive(float x, float y, double time) {
        timer.reset();
        while (timer.seconds() < time) {
            updateAngles();
            drive.arcadeDrive(x, y, PID(false));
            telemetry.addData("", PID(false));
            telemetry.update();
        }
    }

    protected void drive(float x, float y, float z, double time) {
        timer.reset();
        while(timer.seconds() < time) {
            drive.arcadeDrive(x, y, z);
        }
    }

    protected void findLine(float y) {
        while (color_sensor.alpha() < WHITE) {
            updateAngles();
            drive.arcadeDrive(0, y, PID(false));
        }
    }

    private void updateAngles() {
        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }
}
