package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Hardware;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Shooter;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.SHOOTER_SPEED;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.WHITE;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.color_sensor;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.grab;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kD;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kDT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kI;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kIT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kP;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.kPT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.launch;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.wobble;

@TeleOp(name = "Ultimate Goal")
public class UltimateGoalTeleOp extends LinearOpMode {
    //--------------------------------------------------------------
    //Hardware
    //--------------------------------------------------------------
    private final ElapsedTime timer = new ElapsedTime();
    private final DriveTrain drive = new DriveTrain();
    private final Shooter shooter = new Shooter();
    private final Hardware eagle = new Hardware();
    private final Intake intake = new Intake();

    //--------------------------------------------------------------
    //Joystick Values
    //--------------------------------------------------------------
    private float lx, ly, rx;

    private double pos = 0;

    //--------------------------------------------------------------
    //Counters
    //--------------------------------------------------------------
    private int drivers = 2;

    //--------------------------------------------------------------
    //Buttons
    //--------------------------------------------------------------
    private boolean rB = false, dU = false, dD = false, lT = false, a =  false, b = false, startY = false;
    private boolean lastRB = false, lastRB2 = false, lastLB2 = false, lastDU = false, lastDD = false, lastStartY = false, lastB = false;

    private boolean shooterOn = false, off = true;

    //--------------------------------------------------------------
    //Power Shot Variables
    //--------------------------------------------------------------
    protected static double goalAngle;
    private double angleIE, lastAE;
    protected Orientation angles;

    private double lastDegree = 0.0;
    private int rotations = 0;

    @Override
    public void runOpMode() {
        eagle.init(hardwareMap);
        waitForStart();
        while (opModeIsActive()) {
            getJoyValues();
            getButtons();
            updateAngles();

            //--------------------------------------------------------------
            //Drive Train
            //--------------------------------------------------------------
            drive.arcadeDrive(lx, ly, rx);

//            drive.testDrive(lx, ly, rx, getTrueAngle(angles.firstAngle));

            if(rB) {
                if(!lastRB) {
                    drive.zChange();
//                    goalAngle = 135;
//                    timer.reset();
//                    while (timer.seconds() < 2) {
//                        drive.testDrive(.5f, .55f, PID(false), angles.firstAngle);
//                        updateAngles();
//                    }
//                    timer.reset();
//                    while(color_sensor.alpha() < WHITE) {
//                        drive.arcadeDrive(-0.35 * Math.cos(timer.seconds() * Math.PI / 3), 0.65 * Math.sin(timer.seconds() * Math.PI / 3), PID(false));
//                        updateAngles();
//                    }
//                    drive.off();
//                    sleep(500);
//                    while (color_sensor.alpha() < WHITE) {
//                        drive.arcadeDrive(0.0, -0.5, PID(false));
//                        updateAngles();
//                    }
                    lastRB = true;
                }
            } else lastRB = false;

            //--------------------------------------------------------------
            //Shooter
            //--------------------------------------------------------------
            if(shooterOn && !off) shooter.on();
            else shooter.off();

            if (a) launch.setPosition(0.75);
            else launch.setPosition(1.0);

            if (dU && SHOOTER_SPEED < 1.0) {
                if(!lastDU) {
                    SHOOTER_SPEED += 0.01;
                    lastDU = true;
                }
            } else lastDU = false;

            if (dD && SHOOTER_SPEED > 0.0) {
                if(!lastDD) {
                    SHOOTER_SPEED -= 0.01;
                    lastDD = true;
                }
            } else lastDD = false;

            //--------------------------------------------------------------
            //Intake
            //--------------------------------------------------------------
            if (lT) intake.out();
            else if (!shooterOn && !off) intake.in();
            else intake.off();

            //--------------------------------------------------------------
            //Shooter/Intake change
            //--------------------------------------------------------------
            if(gamepad2.right_bumper) {
                if(!lastRB2) {
                    shooterOn = !shooterOn;
                    off = false;
                    lastRB2 = true;
                }
            } else lastRB2 = false;

            if(gamepad2.left_bumper) {
                if(!lastLB2) {
                    off = !off;
                    lastLB2 = true;
                }
            } else lastLB2 = false;

            //--------------------------------------------------------------
            //Change number of drivers
            //--------------------------------------------------------------
            if (startY) {
                if(!lastStartY) {
                    drivers = drivers == 2 ? 1 : 2;
                    lastStartY = true;
                }
            } else lastStartY = false;

            //--------------------------------------------------------------
            //Wobble Goal
            //--------------------------------------------------------------
            wobble.setPower(Range.clip(gamepad2.left_stick_y, -0.75, 0.75));

            if(b) {
                if(!lastB) {
                    pos = pos == 0.0 ? 0.5 : 0.0;
                    grab.setPosition(pos);
                    lastB = true;
                }
            } else lastB = false;

            updateTelemetry();
        }
    }

    private void powerShots() {
        timer.reset();
        while (timer.seconds() < 1.5) {
            drive.arcadeDrive(0, 0, PID(false));
            updateAngles();
        }
    }

    protected float PID(boolean turning) {
        double angleE = goalAngle - getTrueAngle(angles.firstAngle);
        angleIE += angleE;
        double deltaAE = angleE - lastAE;

        double PTerm, ITerm, DTerm;

        if(turning) {
            PTerm = kPT * angleE;
            ITerm = kIT * angleIE;
            DTerm = kDT * deltaAE;
        } else {
            PTerm = kP * angleE;
            ITerm = kI * angleIE;
            DTerm = kD * deltaAE;
        }

        double correction = PTerm + ITerm + DTerm;
        correction = Math.min(0.35, Math.max(-0.35, correction));
        lastAE = angleE;

        if(Math.abs(correction) <= 0.01) return 0.0f;

        return (float)correction * -1;
    }

    private double getTrueAngle(double degree) {
        if(lastDegree > 170 && degree < -170) rotations++;
        else if(lastDegree < -170 && degree > 170) rotations--;
        lastDegree = degree;
        return 360 * rotations + degree;
    }

    private void updateAngles() {
        angles = eagle.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }

    private void getButtons() {
        lT      = drivers == 2 ? gamepad2.left_trigger != 0 : gamepad1.left_trigger != 0;
        dD      = drivers == 2 ? gamepad2.dpad_down : gamepad1.dpad_down;
        dU      = drivers == 2 ? gamepad2.dpad_up : gamepad1.dpad_up;
        a       = drivers == 2 ? gamepad2.a : gamepad1.a;
        b       = drivers == 2 ? gamepad2.b : gamepad1.b;
        startY  = gamepad1.start && gamepad1.y;
        rB      = gamepad1.right_bumper;
    }

    private void getJoyValues() {
        rx = gamepad1.right_stick_x;
        lx = gamepad1.left_stick_x;
        ly = gamepad1.left_stick_y;
    }

    private void updateTelemetry() {
        telemetry.addLine().addData("Number of Drivers ", " " + drivers);
        telemetry.addData("Shooter speed ", " " + SHOOTER_SPEED);
        telemetry.addData("Alpha", color_sensor.alpha());
        telemetry.update();
    }
}
