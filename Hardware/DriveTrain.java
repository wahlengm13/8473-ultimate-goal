package org.firstinspires.ftc.teamcode.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.bL;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.bR;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.fL;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.fR;

public class DriveTrain{
    private int zSpeed = 1;
    public void arcadeDrive(double x, double y, double z) {
        fR.setPower(y + x + z / zSpeed);
        fL.setPower(y - x - z / zSpeed);
        bR.setPower(y - x + z / zSpeed);
        bL.setPower(y + x - z / zSpeed);
    }

    public void arcadeDrive2(float x, float y, float z) {
        final double speed = Math.sqrt(x * x + y * y);
        final double direction = Math.atan2(x, y);

        fR.setPower(speed * Math.sin(direction + Math.PI / 4.0) + z);
        fL.setPower(speed * Math.cos(direction + Math.PI / 4.0) - z);
        bR.setPower(speed * Math.cos(direction + Math.PI / 4.0) + z);
        bL.setPower(speed * Math.sin(direction + Math.PI / 4.0) - z);
    }

    public void testDrive(double x, double y, double z, double angle) {
        final double direction = Math.atan2(x, y) - Math.toRadians(angle);
        final double speed = Math.min(1.0, Math.sqrt(x * x + y * y));

        final double lf = speed * Math.cos(direction + Math.PI / 4.0) - z;
        final double rf = speed * Math.sin(direction + Math.PI / 4.0) + z;
        final double lr = speed * Math.sin(direction + Math.PI / 4.0) - z;
        final double rr = speed * Math.cos(direction + Math.PI / 4.0) + z;

        fR.setPower(rf);
        fL.setPower(lf);
        bR.setPower(rr);
        bL.setPower(lr);
    }

    public void zChange() {
        zSpeed = zSpeed == 1 ? 2 : 1;
    }

    public void holonomic(double x, double y, double z) {
        double r = Math.hypot(x, y);
        double robotAngle = Math.atan2(y, x) - Math.PI / 4;
        final double v1 = r * Math.sin(robotAngle) - z;
        final double v2 = r * Math.cos(robotAngle) + z;
        final double v3 = r * Math.cos(robotAngle) - z;
        final double v4 = r * Math.sin(robotAngle) + z;

        fL.setPower(v1);
        fR.setPower(v2);
        bL.setPower(v3);
        bR.setPower(v4);
    }

    public void off() {
        fR.setPower(0.0);
        fL.setPower(0.0);
        bR.setPower(0.0);
        bL.setPower(0.0);
    }
}