package org.firstinspires.ftc.teamcode.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.SHOOTER_SPEED;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.sR;

public class Shooter {
    public void on() {
        sR.setPower(SHOOTER_SPEED);
    }

    public void off() {
        sR.setPower(0.0);
    }
}