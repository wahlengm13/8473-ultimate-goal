package org.firstinspires.ftc.teamcode.Hardware;

import static org.firstinspires.ftc.teamcode.Hardware.Hardware.iT;
import static org.firstinspires.ftc.teamcode.Hardware.Hardware.iB;

public class Intake {
    public void in() {
        iB.setPower(1.0);
        iT.setPower(1.0);
    }
    public void out() {
        iB.setPower(-1.0);
        iT.setPower(-1.0);
    }
    public void off() {
        iB.setPower(0.0);
        iT.setPower(0.0);
    }
}
