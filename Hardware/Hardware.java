package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

public class Hardware {
    //--------------------------------------------------------------
    //Hardware Objects
    //--------------------------------------------------------------
    public static DcMotor fR, fL, bR, bL;
    public static DcMotor iB, iT;
    public static DcMotor wobble;
    public static DcMotor sR;

    public static Servo launch;
    public static Servo grab;

    //--------------------------------------------------------------
    //Sensors
    //--------------------------------------------------------------
    public static ColorSensor color_sensor;
    public BNO055IMU imu;

    //--------------------------------------------------------------
    //Vuforia/TFOD Stuff
    //--------------------------------------------------------------
    public static VuforiaLocalizer vuforia;
    public static TFObjectDetector tfod;

    public static final String TFOD_MODEL_ASSET = "UltimateGoal.tflite";
    public static final String LABEL_FIRST_ELEMENT = "Quad";
    public static final String LABEL_SECOND_ELEMENT = "Single";
    public static final String VUFORIA_KEY = "AXmoY2r/////AAABmY0tlVphW0SGsQArWjEs8ldkC7C6GILy7DPvwc7IigdmzGM4YX1YQI0OmxppnTwUbsydwC56qMUr7eChk0FW2OT6wgaBFubzM3cV21y8RTaW+jvTSDArdumm96UDX+RSzazSKu6rRpjKpDNG/y6kMWqxg8Yratem85F8tBvNv/cbEFpc10+vuCl72e1FSuVmKihqkrnv3nWz+CZ5DSaCS936W20qhSk9A0zyp8TAgz3IyCKk27X6PypZEjQlMGzhhAI4cqmoOqAFjcbSS3ZhUJCxw0lkanHsDoK2AFrtggjfLEDc+aQJJm0Tr5t8SGKzTwlA3jQlJrLHNTmuGJ2gNX9+oZDDKnhbIDtSwckv2qXe";

    //--------------------------------------------------------------
    //Other Variables
    //--------------------------------------------------------------
    public static final double kP = 0.05, kI = 0.0001, kD = 0.17, kPT = 0.015, kIT = 0.0, kDT = 0.03, kPI = 0.04, kII = 0.0, kDI = 0.08;

    public static double SHOOTER_SPEED = 0.57;

    public static final int WHITE = 60;

    public Hardware() {}

    public void init(HardwareMap hwMap) {
        //--------------------------------------------------------------
        //Motors
        //--------------------------------------------------------------
        fR = hwMap.get(DcMotor.class, "fR");
        fL = hwMap.get(DcMotor.class, "fL");
        bR = hwMap.get(DcMotor.class, "bR");
        bL = hwMap.get(DcMotor.class, "bL");
        sR = hwMap.get(DcMotorEx.class, "sR");
        wobble = hwMap.get(DcMotorEx.class, "sL");
        iB = hwMap.get(DcMotor.class, "iB");
        iT = hwMap.get(DcMotor.class, "iT");

        fL.setDirection(DcMotorSimple.Direction.REVERSE);
        bL.setDirection(DcMotorSimple.Direction.REVERSE);
        iT.setDirection(DcMotorSimple.Direction.REVERSE);
        sR.setDirection(DcMotorSimple.Direction.REVERSE);

        //--------------------------------------------------------------
        //Servos
        //--------------------------------------------------------------
        launch = hwMap.get(Servo.class, "launch");
        grab = hwMap.get(Servo.class, "grab");

        //--------------------------------------------------------------
        //Servos
        //--------------------------------------------------------------
        color_sensor = hwMap.get(ColorSensor.class, "color");

        //--------------------------------------------------------------
        //IMU
        //--------------------------------------------------------------
        BNO055IMU.Parameters IMUparameters = new BNO055IMU.Parameters();
        IMUparameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        IMUparameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        IMUparameters.calibrationDataFile = "BNO055IMUCalibration.json";
        IMUparameters.loggingEnabled      = true;
        IMUparameters.loggingTag          = "IMU";
        IMUparameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        imu = hwMap.get(BNO055IMU.class, "imu");
        imu.initialize(IMUparameters);
    }
}
